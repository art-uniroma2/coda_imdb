package it.uniroma2.art.coda.contracts.imdb;

import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.uima.imdb.type.IMDBActor;
import it.uniroma2.art.uima.imdb.type.IMDBCreator;
import it.uniroma2.art.uima.imdb.type.IMDBDirector;
import it.uniroma2.art.uima.imdb.type.IMDBFilm;
import it.uniroma2.art.uima.imdb.type.IMDBMovie;
import it.uniroma2.art.uima.imdb.type.IMDBPerson;
import it.uniroma2.art.uima.imdb.type.IMDBStar;
import it.uniroma2.art.uima.imdb.type.IMDBTVSeries;
import it.uniroma2.art.uima.imdb.type.IMDBWriter;

public interface IMDBAnnotationContract {
	
	ARTNode produceURI(CODAContext ctx, IMDBMovie imdbMovie);
	ARTNode produceURI(CODAContext ctx, IMDBFilm imdbFilm);
	ARTNode produceURI(CODAContext ctx, IMDBTVSeries imdbtvSeries);
	
	
	ARTNode produceURI(CODAContext ctx, IMDBPerson imdbPerson);
	ARTNode produceURI(CODAContext ctx, IMDBActor imdbActor);
	ARTNode produceURI(CODAContext ctx, IMDBCreator imdbCreator);
	ARTNode produceURI(CODAContext ctx, IMDBDirector imdbDirector);
	ARTNode produceURI(CODAContext ctx, IMDBStar imdbStar);
	ARTNode produceURI(CODAContext ctx, IMDBWriter imdbWriter);
}
