package it.uniroma2.art.coda.converters.imdb;

import it.uniroma2.art.coda.contracts.imdb.IMDBAnnotationContract;
import it.uniroma2.art.coda.interfaces.CODAContext;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.impl.ARTNodeFactoryImpl;
import it.uniroma2.art.uima.imdb.type.IMDBActor;
import it.uniroma2.art.uima.imdb.type.IMDBCreator;
import it.uniroma2.art.uima.imdb.type.IMDBDirector;
import it.uniroma2.art.uima.imdb.type.IMDBFilm;
import it.uniroma2.art.uima.imdb.type.IMDBMovie;
import it.uniroma2.art.uima.imdb.type.IMDBPerson;
import it.uniroma2.art.uima.imdb.type.IMDBSite;
import it.uniroma2.art.uima.imdb.type.IMDBStar;
import it.uniroma2.art.uima.imdb.type.IMDBTVSeries;
import it.uniroma2.art.uima.imdb.type.IMDBWriter;

public class IMDBAnnotationConverter implements IMDBAnnotationContract {

	private ARTNodeFactory fact = new ARTNodeFactoryImpl();

	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBMovie imdbMovie) {
		String title = imdbMovie.getTitle();
		IMDBSite imdbSite = imdbMovie.getMovieSite();
		String site = "";
		if(imdbSite != null){
			site = imdbSite.getSite();
		}
		//now create an id using this information
		String localName = constructLocalName(title, site);
		return fact.createURIResource(ctx.getDefaultNamespace() + localName);
	}
	
	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBFilm imdbFilm) {
		return produceURI(ctx, (IMDBMovie)imdbFilm);
	}

	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBTVSeries imdbtvSeries) {
		return produceURI(ctx, (IMDBMovie)imdbtvSeries);
	}

	
	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBPerson imdbPerson) {
		String name = imdbPerson.getName();
		IMDBSite imdbSite = imdbPerson.getImdbSite();
		String site = "";
		if(imdbSite != null){
			site = imdbSite.getSite();
		}
		//now create an id using this information
		String localName = constructLocalName(name, site);
			
		return fact.createURIResource(ctx.getDefaultNamespace() + localName);
	}
	
	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBActor imdbActor) {
		return produceURI(ctx, (IMDBPerson)imdbActor);
	}

	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBCreator imdbCreator) {
		return produceURI(ctx, (IMDBPerson)imdbCreator);
	}

	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBDirector imdbDirector) {
		return produceURI(ctx, (IMDBPerson)imdbDirector);
	}
	
	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBStar imdbStar) {
		return produceURI(ctx, (IMDBPerson)imdbStar);
	}

	@Override
	public ARTNode produceURI(CODAContext ctx, IMDBWriter imdbWriter) {
		return produceURI(ctx, (IMDBPerson)imdbWriter);
	}	
	
	
	private String constructLocalName(String name, String site){
		String localName = name.replace(" ", "").trim();
		if(site.length() > 0){
			if(site.contains("?")){
				site = site.substring(0, site.indexOf("?"));
			}
			localName += "_"+site.substring(site.length() - 8, site.length() - 1);
		}
		return localName;
	}
	

}
