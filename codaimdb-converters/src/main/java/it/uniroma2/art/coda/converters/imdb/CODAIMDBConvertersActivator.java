package it.uniroma2.art.coda.converters.imdb;

import java.util.Dictionary;
import java.util.Properties;

import it.uniroma2.art.coda.contracts.imdb.ContractConstantsIMDB;
import it.uniroma2.art.coda.contracts.imdb.IMDBAnnotationContract;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class CODAIMDBConvertersActivator implements BundleActivator {
	@Override
	public void start(BundleContext context) throws Exception {
		registerConverter(context, IMDBAnnotationContract.class, "idGen", IMDBAnnotationConverter.class);
		
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// TODO Auto-generated method stub

	}

	private void registerConverter(BundleContext context, Class<?> contractClazz, String contractSimpleName,
			Class<?> converterClazz) throws InstantiationException, IllegalAccessException {
		if (!contractClazz.isAssignableFrom(converterClazz)) {
			throw new IllegalArgumentException(String.format(
					"Contract class %s is not assignable from converter clas %s", contractClazz,
					converterClazz));
		}

		Dictionary<Object, Object> properties = new Properties();
		properties.put("it.uniroma2.art.coda.contract", ContractConstantsIMDB.CODA_CONTRACTS_BASE_URI
				+ contractSimpleName);

		context.registerService(contractClazz.getName(), converterClazz.newInstance(), properties);
	}
}
