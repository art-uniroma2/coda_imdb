package it.uniroma2.art.coda.imdb.exception;

public class GenericTestException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param msg
	 */
	public GenericTestException(String msg) {
        super(msg);
    }
    
    /**
     * 
     * @param e
     */
    public GenericTestException(Exception e) {
        super(e);
    }
}
