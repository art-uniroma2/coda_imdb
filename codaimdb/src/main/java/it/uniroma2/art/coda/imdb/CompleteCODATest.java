package it.uniroma2.art.coda.imdb;

import it.uniroma2.art.coda.core.CODACore;
import it.uniroma2.art.coda.exception.CODAComponentNotSetException;
import it.uniroma2.art.coda.exception.ConverterException;
import it.uniroma2.art.coda.exception.DependencyException;
import it.uniroma2.art.coda.exception.FelixInitializationException;
import it.uniroma2.art.coda.exception.PRParserException;
import it.uniroma2.art.coda.imdb.exception.GenericTestException;
import it.uniroma2.art.coda.imdb.structures.ImdbDBStruct;
import it.uniroma2.art.coda.provisioning.ComponentProvisioningException;
import it.uniroma2.art.coda.standalone.CODAStandaloneFactory;
import it.uniroma2.art.coda.structures.ARTTriple;
import it.uniroma2.art.coda.structures.PreviousDecisionSingleElement;
import it.uniroma2.art.coda.structures.PreviousDecisions;
import it.uniroma2.art.coda.structures.SuggOntologyCoda;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.TransactionBasedModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.BadConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2NonPersistentInMemoryModelConfiguration;
import it.uniroma2.art.uima.imdb.ae.IMDBAnnotator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompleteCODATest {

	private static Logger logger = LoggerFactory.getLogger(CompleteCODATest.class);
	
	private boolean addTripleToOntology = true; // use only for debug purposes

	private String baseUri = "http://my.ontology";
	private String importedUri = "http://my.ontology/imnported";
	private String rdfInputFilePath;
	private String rdfOutputFilePath;
	private String rdfOutputFileBaseName = "OutputComplete_demo_";

	private String imdbListFilePath = "imdbList.txt";

	private boolean useAutocommit;

	private boolean useResoner;

	private boolean recursiveOntologyImport;

	private int stopCont; // when to stop, the number is include (annotate)

	private int whenToSave; // the number of annotatated file after which the ontology is saved to a

	//private String dirComponentsPath = "components";

	private String repositoryDirPath;

	private PreviousDecisions prevDec = new PreviousDecisions();

	//private String oSGiDirPath = "OSGiCache";

	//private String inputFiles = "inputFiles";
	
	//private String tempDir = "tempDir";

	private String prFilePath;
	
	private static long numAddedTriples = 0;

	private String[] uriToAddArray = {};

	private boolean checkuriToAdd = false;
	
	private String rdfFormat;
	
	private static final String PROP_ONTOLOGYFILE = "ontologyFile";
	private static final String PROP_REPOSITORYDIR = "repositoryDir";
	private static final String PROP_PEARLFILE = "pearlFile";
	private static final String PROP_RECURSIVEIMPORT = "recursiveOntologyImport";
	private static final String PROP_NUMOFPAGES = "numOfPages";
	private static final String PROP_SAVEEVERY = "saveEvery";
	private static final String PROP_USEAUTOCOMMIT = "useAutocommit";
	private static final String PROP_USEREASONER = "useResoner";
	private static final String PROP_CONVERTERFOLDER = "converterFolder";
	private static final String PROP_OSGICACHE = "osgiCacheFolder";
	private static final String PROP_INPUTFILES = "inputFiles";
	private static final String PROP_TEMPFOLDER = "tempDir";
	private static final String PROP_RDFFORMAT = "rdfsaveformat";
	

	// use example:
	// java CompleteCODATest codaimdb.properties
	
	public static void main(String args[]) throws Exception {

		if(args.length < 1) {
			System.out.println("Usage: java CompleteCODATest propetyFile");
			System.exit(1);
		}
		

		CompleteCODATest completeExec = new CompleteCODATest();

		OWLModel owlModel = null;
		CODACore codaCore = null;
		long start = System.currentTimeMillis();
		

		try {
			// List<ImdbDBStruct> imdbDBStructList = completeExec.exexuteQuery();
			// List<ImdbDBStruct> imdbDBStructList = completeExec.parseImdbFile();

			Properties prop = completeExec.initializeProperties(args[0]);
			


			// Create an OWLModel and the triple Store.
			owlModel = completeExec.initializeOWLModel();

			// Initialize CODACORE
			codaCore = completeExec.initializeCODACore(null, owlModel, prop.getProperty(PROP_CONVERTERFOLDER),
					prop.getProperty(PROP_OSGICACHE), prop.getProperty(PROP_TEMPFOLDER));

			completeExec.setNumOfPrevDec(-1);

			// Printing the projection rule
//			if (logger.isDebugEnabled())
//				codaCore.getProjRuleModel().printModel();
//			
			

			//int numbOfWebPageAnnForDBEntry;
			// logger.info("number of result = " + imdbDBStructList.size());
			List<String> fileHtmlList = completeExec.getfileHtmlList(prop.getProperty(PROP_INPUTFILES));
			logger.info("number of result = " + fileHtmlList.size());

			// prepare the thread array according to how many thread the programm will have

			// create the Analysis Engine and the JCas
			//System.out.println("descrPathString = " + descrPathString);
			AnalysisEngine ae = completeExec.createAnalysisEngine();
			JCas jcas = completeExec.createJCas(ae);

			completeExec.executeAnnotationAndCODASuggestions(fileHtmlList, owlModel, codaCore, ae, jcas);
			
			

			// Save the ontology
			completeExec.saveRDF(owlModel);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FelixInitializationException e) {
			e.printStackTrace();
		} catch (PRParserException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
		} catch (ModelCreationException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedModelConfigurationException e) {
			e.printStackTrace();
		} catch (UnloadableModelConfigurationException e) {
			e.printStackTrace();
		} catch (AnalysisEngineProcessException e) {
			e.printStackTrace();
		} catch (ResourceInitializationException e) {
			e.printStackTrace();
		} catch (InvalidXMLException e) {
			e.printStackTrace();
		} catch (UnsupportedQueryLanguageException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		} catch (BadConfigurationException e) {
			e.printStackTrace();
		} catch (CODAComponentNotSetException e) {
			e.printStackTrace();
		} finally {
			try {
				if (codaCore != null)
					codaCore.stopAndClose();
			} catch (ModelUpdateException e) {
				e.printStackTrace();
			}
			long end = System.currentTimeMillis();
			String prettyTime = "Program terminated after " + completeExec.printPrettyTime(start, end);
			String numAddedTriplesString = "Program added " + completeExec.printPrettyNumber(numAddedTriples) + " RDF triples";
			logger.info(prettyTime);
			logger.info(numAddedTriplesString);
		}

	}

	

	private Properties initializeProperties(String propFile) throws FileNotFoundException, IOException {
		File fileProp = new File(propFile);
		Properties prop = new Properties();
		prop.load(new FileInputStream(fileProp));
				
		rdfInputFilePath = prop.getProperty(PROP_ONTOLOGYFILE);
		repositoryDirPath = prop.getProperty(PROP_REPOSITORYDIR);
		prFilePath = prop.getProperty(PROP_PEARLFILE);
		recursiveOntologyImport = 
				(prop.getProperty(PROP_RECURSIVEIMPORT).compareTo("true") == 0) ? true : false;
		stopCont = Integer.parseInt(prop.getProperty(PROP_NUMOFPAGES));
		whenToSave = Integer.parseInt(prop.getProperty(PROP_SAVEEVERY));
		useAutocommit = (prop.getProperty(PROP_USEAUTOCOMMIT).compareTo("true") == 0) ? true : false;
		useResoner = (prop.getProperty(PROP_USEREASONER).compareTo("true") == 0) ? true : false;
		
		String targetBundlesFolderString = prop.getProperty(PROP_CONVERTERFOLDER);
		String targetFelixCacheString = prop.getProperty(PROP_OSGICACHE);
		
		rdfFormat = prop.getProperty(PROP_RDFFORMAT);
		
		String prFilePathLocal = (new File(prFilePath)).getName();
		
		rdfOutputFilePath = rdfOutputFileBaseName + prFilePathLocal +"_"+ 
				(useAutocommit==true?"t":"f") + (useResoner==true?"t":"f") + ".owl";

		logger.info("rdfInputFilePath = " + rdfInputFilePath);
		logger.info("repositoryDirPath = " + repositoryDirPath);
		logger.info("prFilePath = " + prFilePath);
		logger.info("recursiveOntologyImport = " + recursiveOntologyImport);
		logger.info("stopCont = " + stopCont);
		logger.info("whenToSave = " + whenToSave);
		logger.info("useAutocommit = " + useAutocommit);
		logger.info("useResoner = " + useResoner);

		logger.info("targetBundlesFolderString = " + targetBundlesFolderString);
		logger.info("targetFelixCacheString = " + targetFelixCacheString);
		
		logger.info("rdfFormat = "+rdfFormat);
		return prop;
		
	}
	
	private void executeAnnotationAndCODASuggestions(List<String> fileHtmlList, OWLModel owlModel, 
			CODACore codaCore, AnalysisEngine ae, JCas jcas) throws IOException, ModelAccessException, 
			UnsupportedRDFFormatException, AnalysisEngineProcessException, PRParserException, 
			ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException, 
			MalformedQueryException, QueryEvaluationException, DependencyException, ModelUpdateException {
		int cont = 0;
		int startCont = 1;
		
		for (String fileHtmlPath : fileHtmlList) {

			clearAllPrevDec();
			++cont;
			if (cont < startCont)
				continue;

			if (cont % whenToSave == 0) {
				// Save the ontology
				saveRDF(owlModel);
			}

			if (cont > stopCont)
				break;

			String textToBeAnnotatate = getTextToBeAnnotatedFromFile(fileHtmlPath);

			// use the UIMA AAE to annotate the text
			logger.info(cont + " annotating file = " + fileHtmlPath);
			analyzeNewText(ae, jcas, textToBeAnnotatate);

			// set the jcas in CODA
			codaCore.setJCas(jcas);

			// process all the UIMA annotation
			long numAddedTriplesTemp = processUIMAAnnotation(codaCore, owlModel);
			numAddedTriples += numAddedTriplesTemp;

			/*
			 * // old versione with internet connection completeExec.clearAllPrevDec();
			 * 
			 * numbOfWebPageAnnForDBEntry = 0; ++cont;
			 * 
			 * if (cont < startCont) continue;
			 * 
			 * if (cont % 10 == 0) { // Save the ontology completeExec.saveRDF(owlModel); } if (cont >
			 * stopCont) break;
			 * 
			 * // String sitoIMDBFromDB = imdbDBStruct.getSitoIMDBFromDB(); // int voto =
			 * imdbDBStruct.getVoto(); // int voto = 2; // String varie = imdbDBStruct.getVarie(); // int
			 * numDVD = imdbDBStruct.getNumDVD();
			 * 
			 * // if (sitoIMDBFromDB == null || sitoIMDBFromDB.compareTo("") == 0) // continue;
			 * 
			 * while (numbOfWebPageAnnForDBEntry < 2) { String sitoIMDB; ++numbOfWebPageAnnForDBEntry; if
			 * (numbOfWebPageAnnForDBEntry == 2) sitoIMDB = sitoIMDBFromDB + "fullcredits#cast"; else
			 * sitoIMDB = sitoIMDBFromDB;
			 * 
			 * String textToBeAnnotatate = ""; int numTry = 0; while (textToBeAnnotatate.compareTo("") ==
			 * 0 && numTry < 4) { textToBeAnnotatate = completeExec.getTextToBeAnnotated(sitoIMDB);
			 * ++numTry; } if (textToBeAnnotatate.compareTo("") == 0) { logger.info(cont +
			 * " problem with " + sitoIMDB); continue; // it was not possible to retrieve the page, even
			 * after 4 attempt } // use the UIMA AAE to annotate the text logger.info(cont +
			 * " annotating file = " + sitoIMDB); // JCas annotCas =
			 * completeExec.analyzeText(descrPathString, textToBeAnnotatate);
			 * completeExec.analyzeNewText(ae, jcas, textToBeAnnotatate);
			 * 
			 * // set the jcas in CODA codaCore.setJCas(jcas);
			 * 
			 * // process all the UIMA annotation completeExec.processUIMAAnnotation(codaCore, owlModel);
			 * 
			 * // if (numbOfWebPageAnnForDBEntry == 2) // completeExec.addOtherInfoToRDF(owlModel,
			 * sitoIMDBFromDB, voto, varie, numDVD); }
			 */
		}
		
	}

	public List<ImdbDBStruct> parseImdbFile() throws GenericTestException {
		List<ImdbDBStruct> imdbDBStructList = new ArrayList<ImdbDBStruct>();
		try {

			File file = new File(imdbListFilePath);
			BufferedReader bf = new BufferedReader(new FileReader(file));
			String line;
			while ((line = bf.readLine()) != null) {
				if (!line.startsWith("http"))
					continue;
				imdbDBStructList.add(new ImdbDBStruct(line, 3, "", 1));
			}
		} catch (FileNotFoundException e) {
			throw new GenericTestException(e);
		} catch (IOException e) {
			throw new GenericTestException(e);
		}
		return imdbDBStructList;
	}

	public List<String> getfileHtmlList(String inputFiles) {
		List<String> fileHtmlList = new ArrayList<String>();
		File dir = new File(inputFiles);
		File fileArray[] = dir.listFiles();
		for (int i = 0; i < fileArray.length; ++i) {
			fileHtmlList.add(fileArray[i].getAbsolutePath());
		}
		return fileHtmlList;
	}

	private boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	/*private String getTextToBeAnnotated(String htmlFilePath) throws IOException {
		// read the input file containig the test to be analized
		HttpURLConnection httpcon = null;
		StringBuilder contents = new StringBuilder();
		try {
			logger.debug("requesting imdb page");
			URL url = new URL(htmlFilePath);
			httpcon = (HttpURLConnection) url.openConnection();
			httpcon.setRequestMethod("GET");
			httpcon.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");

			httpcon.setDoOutput(true);
			httpcon.setConnectTimeout(10 * 1000);
			httpcon.setReadTimeout(10 * 1000);
			httpcon.connect();

			InputStreamReader inputStreamReader = new InputStreamReader(httpcon.getInputStream());
			BufferedReader br = new BufferedReader(inputStreamReader);

			// contents = new StringBuilder();
			String line;

			while ((line = br.readLine()) != null) {
				contents.append(line);
				contents.append(System.getProperty("line.separator"));
			}
		} catch (MalformedURLException e) {
			contents.delete(0, contents.length());
			contents.append("");
			logger.debug("MalformedURLException with: " + htmlFilePath);
			// e.printStackTrace();
		} catch (ProtocolException e) {
			contents.delete(0, contents.length());
			contents.append("");
			logger.debug("ProtocolException with: " + htmlFilePath);
			// e.printStackTrace();
		} catch (IOException e) {
			contents.delete(0, contents.length());
			contents.append("");
			logger.debug("IOException with: " + htmlFilePath);
			// e.printStackTrace();
		} finally {
			// close the connection, set all objects to null
			httpcon.disconnect();
			httpcon.setRequestProperty("Connection", "close");
		}
		logger.debug("imdb page obtained");
		if (contents.toString().compareTo("") == 0) {
			logger.debug("the imdb page " + htmlFilePath + " content is null");
			return "";
		}
		return contents.toString();
	}*/

	private String getTextToBeAnnotatedFromFile(String fileHtmlPath) {
		StringBuilder contents = new StringBuilder();
		try {
			File file = new File(fileHtmlPath);
			FileInputStream fileInputStream;
			fileInputStream = new FileInputStream(file);
			InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
			BufferedReader br = new BufferedReader(inputStreamReader);
			String line;

			while ((line = br.readLine()) != null) {
				contents.append(line);
				contents.append(System.getProperty("line.separator"));
			}
		} catch (FileNotFoundException e) {
			logger.debug("FileNotFoundException with: " + fileHtmlPath);
		} catch (IOException e) {
			logger.debug("IOException with: " + fileHtmlPath);
		}
		return contents.toString();

	}

	private AnalysisEngine createAnalysisEngine()
			throws ResourceInitializationException, InvalidXMLException, IOException {
		AnalysisEngine ae = AnalysisEngineFactory.createEngine(IMDBAnnotator.class);
		
		return ae;
	}

	private JCas createJCas(AnalysisEngine ae) throws UIMAException {
		JCas jcas = JCasFactory.createJCas();
		return jcas;
	}

	private void analyzeNewText(AnalysisEngine ae, JCas jcas, String textToBeAnnotatate)
			throws AnalysisEngineProcessException {
		jcas.reset();
		jcas.setDocumentText(textToBeAnnotatate);
		
		//ae.process(jcas);
		SimplePipeline.runPipeline(jcas, ae);
	}

	/*
	 * private JCas analyzeText(String descrPathString, String textToBeAnnotatate) throws IOException,
	 * ResourceInitializationException, AnalysisEngineProcessException, InvalidXMLException { // pass the txt
	 * read to the AE (which is an AAE). To do this read the AAE descriptor and then execute // the AAE //
	 * XMLInputSource in; ResourceSpecifier specifier; AnalysisEngine ae; JCas annotCas;
	 * 
	 * // in = new XMLInputSource(aggregateAEFilePath); // in = new XMLInputSource(descrPathString); //
	 * specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(in); // specifier =
	 * UIMAFramework.getXMLParser().parseResourceSpecifier(new // XMLInputSource(descrPathString));
	 * 
	 * AnalysisEngineDescription aeDesc = UIMAFramework.getXMLParser().parseAnalysisEngineDescription( new
	 * XMLInputSource(descrPathString)); ae = UIMAFramework.produceAnalysisEngine(aeDesc);
	 * 
	 * // ae = UIMAFramework.produceAnalysisEngine(specifier); ae.setConfigParameterValue("Encoding",
	 * "UTF-8"); annotCas = ae.newJCas(); annotCas.setDocumentText(textToBeAnnotatate); ae.process(annotCas);
	 * return annotCas; }
	 */

	private OWLModel initializeOWLModel() throws ModelUpdateException, FileNotFoundException, IOException,
			ModelAccessException, UnsupportedRDFFormatException, ClassNotFoundException,
			ModelCreationException, InstantiationException, IllegalAccessException,
			UnsupportedModelConfigurationException, UnloadableModelConfigurationException,
			BadConfigurationException {

		ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();
		OWLArtModelFactory<Sesame2ModelConfiguration> fact = OWLArtModelFactory.createModelFactory(factImpl);
		
		
		Sesame2ModelConfiguration modelConf = factImpl
				.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);
		//Sesame2ModelConfiguration modelConf = factImpl
		//		.createModelConfigurationObject(Sesame2PersistentInMemoryModelConfiguration.class);
		
		
		
		if (!useResoner) {
			modelConf.setParameter("rdfsInference", false);
			modelConf.setParameter("directTypeInference", false);
		}

		File repDirFile = new File(repositoryDirPath);
		if(!repDirFile.exists()){
			repDirFile.mkdir();
		}
		
		OWLModel owlModel = fact.loadOWLModel(baseUri, repositoryDirPath, modelConf);

		owlModel.setBaseURI(baseUri);
		owlModel.setDefaultNamespace(baseUri + "#");
		// owlModel.clearRDF();
		owlModel.addRDF(new File(rdfInputFilePath), importedUri, RDFFormat.RDFXML, NodeFilters.MAINGRAPH);

		ArrayList<String> importedOntologiesList = new ArrayList<String>();
		if(recursiveOntologyImport)
			recursiveImport(owlModel, importedOntologiesList);

		return owlModel;
	}

	private void recursiveImport(OWLModel owlModel, ArrayList<String> importedOntologiesList)
			throws ModelAccessException, FileNotFoundException, MalformedURLException, IOException,
			ModelUpdateException, UnsupportedRDFFormatException {
		ARTURIResourceIterator artUriIter = owlModel.listOntologyImports(NodeFilters.ANY, NodeFilters.ANY);
		while (artUriIter.hasNext()) {
			ARTURIResource artUriRes = artUriIter.next();
			String uriString = artUriRes.getURI();
			if (!importedOntologiesList.contains(uriString)) {
				importedOntologiesList.add(uriString);
				System.out.println("importing ontology: " + uriString);
				owlModel.addRDF(new URL(artUriRes.getURI()), importedUri, RDFFormat.RDFXML,
						NodeFilters.MAINGRAPH);
				recursiveImport(owlModel, importedOntologiesList);
			}
		}
	};

	private CODACore initializeCODACore(JCas annotCas, OWLModel owlModel, String targetBundlesFolderString,
			String targetFelixCacheString, String tempDirString)
			throws Exception {
		
		File targetBundlesFolder = new File(targetBundlesFolderString);
		File targetFelixCache = new File(targetFelixCacheString);
		targetFelixCache.mkdirs();
		
		Properties otherProp = new Properties();
		otherProp.put(CODAStandaloneFactory.CODA_EXPORT_PACKAGES_PROPERTY_NAME, 
				"it.uniroma2.art.uima.imdb.type");
		
		CODACore codaCore = CODAStandaloneFactory.getInstance(targetBundlesFolder, targetFelixCache, 
				otherProp);
		
		PreviousDecisions prevDecision = new PreviousDecisions();
		File tempDir = new File(tempDirString);
		//codaCore.initialize(owlModel, prevDecision, tempDir); // OLd
		codaCore.initialize(owlModel, new ARTModelFactorySesame2Impl());
		
		return codaCore;
	}


	private long processUIMAAnnotation(CODACore codaCore, OWLModel owlModel) throws PRParserException, 
			ComponentProvisioningException, ConverterException, UnsupportedQueryLanguageException, 
			ModelAccessException, MalformedQueryException, QueryEvaluationException, DependencyException, 
			ModelUpdateException {
		long numAddedTriples = 0;
		SuggOntologyCoda suggOntCoda;
		
		codaCore.setProjectionRulesModel(new File(prFilePath));
		
		while (codaCore.isAnotherAnnotationPresent()) {
			logger.debug("\n\n");
			
			suggOntCoda = codaCore.processNextAnnotation();
			

			logger.debug(
					" type: " + suggOntCoda.getAnnotation().getType().toString() + "\t"
							+ suggOntCoda.getAnnotation().getBegin() + " -> "
							+ suggOntCoda.getAnnotation().getEnd());

			// add the suggestion and the just added triple to the prevDec
			List<ARTTriple> addedTripleList = new ArrayList<ARTTriple>();
			if (!useAutocommit)
				((TransactionBasedModel) owlModel).setAutoCommit(false);
			for (ARTTriple artTriple : suggOntCoda.getAllARTTriple()) {
				ARTResource subject = artTriple.getSubject();
				ARTURIResource predicate = artTriple.getPredicate();
				ARTNode object = artTriple.getObject();
				
				if(addTripleToOntology){
					owlModel.addTriple(subject, predicate, object);
				}
				numAddedTriples++;
				addedTripleList.add(artTriple);
			}
			if (!useAutocommit)
				((TransactionBasedModel) owlModel).commit();
			PreviousDecisionSingleElement prevDecSingleElem = new PreviousDecisionSingleElement(
					addedTripleList, suggOntCoda);
			prevDec.addPrevDecSingleElem(prevDecSingleElem);
		}
		return numAddedTriples;
	}

	private void saveRDF(OWLModel owlModel) throws IOException, ModelAccessException,
			UnsupportedRDFFormatException {
		logger.info("saving rdf to : " + rdfOutputFilePath); // convert to DEBUG
		
		RDFFormat rdfFormatSave = getRDFFormat(rdfFormat);
		owlModel.writeRDF(new File(rdfOutputFilePath), rdfFormatSave, NodeFilters.MAINGRAPH);
	}

	private RDFFormat getRDFFormat(String rdfFormatInput){
		RDFFormat rdfFormatResult;
		if(rdfFormatInput.compareToIgnoreCase("turtle") == 0){
			rdfFormatResult = RDFFormat.TURTLE;
		} else if(rdfFormatInput.compareToIgnoreCase("n3") == 0){
			rdfFormatResult = RDFFormat.N3;
		} else if(rdfFormatInput.compareToIgnoreCase("rdfxml") == 0){
			rdfFormatResult = RDFFormat.RDFXML;
		} else if(rdfFormatInput.compareToIgnoreCase("trix") == 0){
			rdfFormatResult = RDFFormat.TRIX;
		} else {
			rdfFormatResult = RDFFormat.TURTLE;
		}
		return rdfFormatResult;
	}
	
	public void clearAllPrevDec() {
		prevDec.clearAllPrevDec();
	}

	public void setNumOfPrevDec(int numPrevDec) {
		prevDec.setMaxPrevDev(numPrevDec);
	}


	public String printPrettyTime(long start, long end) {
		long secTotal = (end - start) / 1000;
		long sec = secTotal % 60;
		long minTotal = secTotal / 60;
		long min = minTotal % 60;
		long hourTotal = minTotal / 60;

		String prettyTime = "";
		if (hourTotal < 10)
			prettyTime += "0";
		prettyTime += hourTotal + ":";
		if (min < 10)
			prettyTime += "0";
		prettyTime += min + ":";
		if (sec < 10)
			prettyTime += "0";
		prettyTime += sec;

		return prettyTime;
	}
	
	/*public String printPrettyNumber(long number){
		String prettyString="";
		long temp=number;
		long remainder = 0;
		while(temp/1000 != 0) {
			remainder = temp % 1000;
			prettyString += "."+remainder+prettyString;
			temp /= 1000;
		}
		remainder = temp % 1000;
		prettyString = +remainder+prettyString;
		return prettyString;
	}*/
	
	public String printPrettyNumber(long number){
		String temp = String.valueOf(number);
		String numberString="";
		int count = 0;
		for(int i=temp.length()-1; i>=0; --i){
			if(count % 3 == 0 && count != 0){
				numberString = temp.charAt(i)+"."+numberString;
			} else{
				numberString = temp.charAt(i)+numberString;
			}
			++count;
		}
		return numberString;
	}

}
