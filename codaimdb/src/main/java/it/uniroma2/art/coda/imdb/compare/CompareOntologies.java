package it.uniroma2.art.coda.imdb.compare;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTStatement;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.navigation.ARTStatementIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2NonPersistentInMemoryModelConfiguration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompareOntologies {
	
	private static Logger logger = LoggerFactory.getLogger(CompareOntologies.class);
	
	private static String PROP_ONTOLOGYAFILE = "ontologyAFile";
	private static String PROP_ONTOLOGYBFILE = "ontologyBFile";
	private static String PROP_RDFLOADFORMATA = "rdfloadformatA";
	private static String PROP_RDFLOADFORMATB = "rdfloadformatB";
	private static String PROP_BASEURI = "baseuri";
	private static String PROP_REPADIRPATH = "repositoryADir";
	private static String PROP_REPBDIRPATH = "repositoryBDir";
	private static String PROP_OUTPUTFILE = "outputFile";
	private static String PROP_EXCLUDEBNODE = "excludeBNode";
	
	
	private String ontologyAFile;
	private String ontologyBFile;
	
	private String rdfFormatA;
	private String rdfFormatB;
	
	private OWLModel owlModelA;
	private OWLModel owlModelB;
	private String baseUri;
	
	private String repositoryADirPath;
	private String repositoryBDirPath;
	
	private Map<String, Map<String, List<String>>> rdfTriplesAMap; 
	private Map<String, Map<String, List<String>>> rdfTriplesBMap; 
	
	private List<String> rdfTripleAList;
	private List<String> rdfTripleBList;
	
	private List<String> missingTriplesInA;
	private List<String> missingTriplesInB;
	
	private String outputFile;
	
	private boolean excludeBNode;
	
	private boolean useListInsteadOfMap = false; // used only for debug/performance reason
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if(args.length < 1) {
			System.out.println("Usage: java CompareOntologies propetyFile");
			System.exit(1);
		}
		
		CompareOntologies compareOntologies = new CompareOntologies();
		
		try {
			compareOntologies.initializeProperties(args[0]);
			
			compareOntologies.initializeOWLModels();
			
			long start = System.currentTimeMillis();
			compareOntologies.getAlltriplesFromOntologies();
			
			compareOntologies.compareTriples();
			long end = System.currentTimeMillis();
			logger.info("Comparison took "+(end-start)+" millisec ");
			compareOntologies.saveResults();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ModelAccessException e) {
			e.printStackTrace();
		} catch (ModelUpdateException e) {
			e.printStackTrace();
		} catch (UnsupportedRDFFormatException e) {
			e.printStackTrace();
		} catch (UnsupportedModelConfigurationException e) {
			e.printStackTrace();
		} catch (UnloadableModelConfigurationException e) {
			e.printStackTrace();
		} catch (ModelCreationException e) {
			e.printStackTrace();
		}

	}


	private void initializeProperties(String propFile) throws FileNotFoundException, IOException {
		logger.info("initializeProperties");
		File fileProp = new File(propFile);
		Properties prop = new Properties();
		prop.load(new FileInputStream(fileProp));
				
		ontologyAFile = prop.getProperty(PROP_ONTOLOGYAFILE);
		ontologyBFile = prop.getProperty(PROP_ONTOLOGYBFILE);
		
		rdfFormatA = prop.getProperty(PROP_RDFLOADFORMATA);
		rdfFormatB = prop.getProperty(PROP_RDFLOADFORMATB);
		
		baseUri = prop.getProperty(PROP_BASEURI);
		
		repositoryADirPath = prop.getProperty(PROP_REPADIRPATH);
		repositoryBDirPath = prop.getProperty(PROP_REPBDIRPATH);
		
		excludeBNode = Boolean.parseBoolean(prop.getProperty(PROP_EXCLUDEBNODE, "true"));
		
		outputFile = prop.getProperty(PROP_OUTPUTFILE);

	}

	
	private void initializeOWLModels() throws FileNotFoundException, MalformedURLException, 
			ModelAccessException, IOException, ModelUpdateException, UnsupportedRDFFormatException, 
			UnsupportedModelConfigurationException, UnloadableModelConfigurationException, 
			ModelCreationException {
		logger.info("initializeOWLModels");
		// OWL MODEL A
		ARTModelFactorySesame2Impl factImplA = new ARTModelFactorySesame2Impl();
		OWLArtModelFactory<Sesame2ModelConfiguration> factA = OWLArtModelFactory.createModelFactory(factImplA);
		Sesame2ModelConfiguration modelConf = factImplA
				.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);

		File repADirFile = new File(repositoryADirPath);
		if(!repADirFile.exists()){
			repADirFile.mkdir();
		}
		owlModelA = factA.loadOWLModel(baseUri, repositoryADirPath, modelConf);

		owlModelA.setBaseURI(baseUri);
		owlModelA.setDefaultNamespace(baseUri + "#");
		// owlModel.clearRDF();
		RDFFormat rdfFormatALoad = getRDFFormat(rdfFormatA);
		owlModelA.addRDF(new File(ontologyAFile), baseUri, rdfFormatALoad, NodeFilters.MAINGRAPH);

		ArrayList<String> importedOntologiesAList = new ArrayList<String>();
		recursiveImport(owlModelA, importedOntologiesAList);
		logger.info("Initialized OWL Model A");
		
		// OWL MODEL B
		ARTModelFactorySesame2Impl factImplB = new ARTModelFactorySesame2Impl();
		OWLArtModelFactory<Sesame2ModelConfiguration> factB = OWLArtModelFactory.createModelFactory(factImplB);
		Sesame2ModelConfiguration modelConfB = factImplB
				.createModelConfigurationObject(Sesame2NonPersistentInMemoryModelConfiguration.class);

		File repBDirFile = new File(repositoryBDirPath);
		if(!repBDirFile.exists()){
			repBDirFile.mkdir();
		}
		owlModelB = factB.loadOWLModel(baseUri, repositoryBDirPath, modelConf);

		owlModelB.setBaseURI(baseUri);
		owlModelB.setDefaultNamespace(baseUri + "#");
		// owlModel.clearRDF();
		RDFFormat rdfFormatBLoad = getRDFFormat(rdfFormatB);
		owlModelB.addRDF(new File(ontologyBFile), baseUri, RDFFormat.TURTLE, NodeFilters.MAINGRAPH);

		ArrayList<String> importedOntologiesBList = new ArrayList<String>();
		recursiveImport(owlModelA, importedOntologiesBList);
		logger.info("Initialized OWL Model A");
	}
	
	private RDFFormat getRDFFormat(String rdfFormatInput){
		RDFFormat rdfFormatResult;
		if(rdfFormatInput.compareToIgnoreCase("turtle") == 0){
			rdfFormatResult = RDFFormat.TURTLE;
		} else if(rdfFormatInput.compareToIgnoreCase("n3") == 0){
			rdfFormatResult = RDFFormat.N3;
		} else if(rdfFormatInput.compareToIgnoreCase("rdfxml") == 0){
			rdfFormatResult = RDFFormat.RDFXML;
		} else if(rdfFormatInput.compareToIgnoreCase("trix") == 0){
			rdfFormatResult = RDFFormat.TRIX;
		} else {
			rdfFormatResult = RDFFormat.TURTLE;
		}
		return rdfFormatResult;
	}
	
	private void recursiveImport(OWLModel owlModel, ArrayList<String> importedOntologiesList)
			throws ModelAccessException, FileNotFoundException, MalformedURLException, IOException,
			ModelUpdateException, UnsupportedRDFFormatException {
		ARTURIResourceIterator artUriIter = owlModel.listOntologyImports(NodeFilters.ANY, NodeFilters.ANY);
		while (artUriIter.hasNext()) {
			ARTURIResource artUriRes = artUriIter.next();
			String uriString = artUriRes.getURI();
			if (!importedOntologiesList.contains(uriString)) {
				importedOntologiesList.add(uriString);
				System.out.println("importing ontology: " + uriString);
				owlModel.addRDF(new URL(artUriRes.getURI()), baseUri, RDFFormat.RDFXML,
						NodeFilters.MAINGRAPH);
				recursiveImport(owlModel, importedOntologiesList);
			}
		}
	}
	
	private void getAlltriplesFromOntologies() throws ModelAccessException{
		logger.info("getAlltriplesFromOntologies");
		//OWL MODEL A
		if(useListInsteadOfMap){
			ARTStatementIterator iterA = owlModelA.listStatements(NodeFilters.ANY, NodeFilters.ANY, 
					NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
			rdfTripleAList = new ArrayList<String>();
			while(iterA.hasNext()){
				ARTStatement stat = iterA.getNext();
				ARTResource subj = stat.getSubject();
				ARTURIResource pred = stat.getPredicate();
				ARTNode obj = stat.getObject();
				if(excludeBNode){
					//exclude the BNODE
					if(subj.isBlank() || pred.isBlank() || obj.isBlank()){
						continue;
					}
				}
				rdfTripleAList.add(subj.getNominalValue()+"\t"+pred.getNominalValue()+"\t"+
						obj.getNominalValue());
			}
			iterA.close();
			logger.info("rdfTripleAList with "+rdfTripleAList.size()+" differnet subj");
		}else{
			ARTStatementIterator iterA = owlModelA.listStatements(NodeFilters.ANY, NodeFilters.ANY, 
					NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
			rdfTriplesAMap = new HashMap<String, Map<String,List<String>>>();
			while(iterA.hasNext()){
				ARTStatement stat = iterA.getNext();
				ARTResource subj = stat.getSubject();
				ARTURIResource pred = stat.getPredicate();
				ARTNode obj = stat.getObject();
				if(excludeBNode){
					//exclude the BNODE
					if(subj.isBlank() || pred.isBlank() || obj.isBlank()){
						continue;
					}
						
				}
				if(!rdfTriplesAMap.containsKey(subj.getNominalValue())){
					rdfTriplesAMap.put(subj.getNominalValue(), new HashMap<String, List<String>>());
				}
				Map<String, List<String>> predObjMap = rdfTriplesAMap.get(subj.getNominalValue());
				if(!predObjMap.containsKey(pred.getNominalValue())){
					predObjMap.put(pred.getNominalValue(), new ArrayList<String>());
				}
				List<String> objList = predObjMap.get(pred.getNominalValue());
				if(!objList.contains(obj.getNominalValue())){
					objList.add(obj.getNominalValue());
				}
			}
			iterA.close();
			logger.info("rdfTriplesAMap with "+rdfTriplesAMap.size()+" differnet subj");
		}
		//OWL MODEL B
		if(useListInsteadOfMap){
			ARTStatementIterator iterB = owlModelB.listStatements(NodeFilters.ANY, NodeFilters.ANY,
					NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
			rdfTripleBList = new ArrayList<String>();
			while (iterB.hasNext()) {
				ARTStatement stat = iterB.getNext();
				ARTResource subj = stat.getSubject();
				ARTURIResource pred = stat.getPredicate();
				ARTNode obj = stat.getObject();
				if(excludeBNode){
					//exclude the BNODE
					if(subj.isBlank() || pred.isBlank() || obj.isBlank()){
						continue;
					}
				}
				rdfTripleBList.add(subj.getNominalValue()+"\t"+pred.getNominalValue()+"\t"+
						obj.getNominalValue());
			}
			iterB.close();
			logger.info("rdfTripleBList with "+rdfTripleBList.size()+" differnet subj");
		} else {
			ARTStatementIterator iterB = owlModelB.listStatements(NodeFilters.ANY, NodeFilters.ANY,
					NodeFilters.ANY, false, NodeFilters.MAINGRAPH);
			rdfTriplesBMap = new HashMap<String, Map<String, List<String>>>();
			while (iterB.hasNext()) {
				ARTStatement stat = iterB.getNext();
				ARTResource subj = stat.getSubject();
				ARTURIResource pred = stat.getPredicate();
				ARTNode obj = stat.getObject();
				if(excludeBNode){
					//exclude the BNODE
					if(subj.isBlank() || pred.isBlank() || obj.isBlank()){
						continue;
					}
				}
				if (!rdfTriplesBMap.containsKey(subj.getNominalValue())) {
					rdfTriplesBMap.put(subj.getNominalValue(), new HashMap<String, List<String>>());
				}
				Map<String, List<String>> predObjMap = rdfTriplesBMap.get(subj.getNominalValue());
				if (!predObjMap.containsKey(pred.getNominalValue())) {
					predObjMap.put(pred.getNominalValue(), new ArrayList<String>());
				}
				List<String> objList = predObjMap.get(pred.getNominalValue());
				if (!objList.contains(obj.getNominalValue())) {
					objList.add(obj.getNominalValue());
				}
			}
			iterB.close();
			logger.info("rdfTriplesBMap with "+rdfTriplesBMap.size()+" differnet subj");
		}
	}
	
	
	
	private void compareTriples(){
		logger.info("compareTriples");
		//search for triples in Ontology A not present in Ontology B
		missingTriplesInB = new ArrayList<String>();
		if(useListInsteadOfMap){
			for(String tripleA : rdfTripleAList){
				if(!rdfTripleBList.contains(tripleA)){
					addTripleToMissingList(tripleA, missingTriplesInB);
				}
			}
		} else {
			for(String subj : rdfTriplesAMap.keySet()){
				Map<String, List<String>> predObjMapA = rdfTriplesAMap.get(subj);
				for(String pred : predObjMapA.keySet()){
					List<String> objListA = predObjMapA.get(pred);
					for(String obj : objListA){
						if(!rdfTriplesBMap.containsKey(subj)){
							addTripleToMissingList(subj, pred, obj, missingTriplesInB);
						} else{
							Map<String, List<String>> predObjMapB = rdfTriplesBMap.get(subj);
							if(!predObjMapB.containsKey(pred)){
								addTripleToMissingList(subj, pred, obj, missingTriplesInB);
							} else{
								List<String> objListB = predObjMapB.get(pred);
								if(!objListB.contains(obj)){
									addTripleToMissingList(subj, pred, obj, missingTriplesInB);
								}
							}
						}
					}
				}
			}
		}
		logger.info("Compared Ontology A with Ontology B, found "+missingTriplesInB.size()+" triples in " +
				"Ontology A not present in Ontology B");
		//search for triples in Ontology B not present in Ontology A
		missingTriplesInA = new ArrayList<String>();
		if(useListInsteadOfMap){
			for(String tripleB : rdfTripleBList){
				if(!rdfTripleAList.contains(tripleB)){
					addTripleToMissingList(tripleB, missingTriplesInA);
				}
			}
		} else {
			for (String subj : rdfTriplesBMap.keySet()) {
				Map<String, List<String>> predObjMapB = rdfTriplesBMap.get(subj);
				for (String pred : predObjMapB.keySet()) {
					List<String> objListB = predObjMapB.get(pred);
					for (String obj : objListB) {
						if (!rdfTriplesAMap.containsKey(subj)) {
							addTripleToMissingList(subj, pred, obj, missingTriplesInA);
						} else {
							Map<String, List<String>> predObjMapA = rdfTriplesAMap.get(subj);
							if (!predObjMapA.containsKey(pred)) {
								addTripleToMissingList(subj, pred, obj, missingTriplesInA);
							} else {
								List<String> objListA = predObjMapA.get(pred);
								if (!objListA.contains(obj)) {
									addTripleToMissingList(subj, pred, obj, missingTriplesInA);
								}
							}
						}
					}
				}
			}
		}
		logger.info("Compared Ontology B with Ontology A, found "+missingTriplesInA.size()+" triples in " +
				"Ontology B not present in Ontology A");
	}
	
	private void addTripleToMissingList(String subj, String pred, String obj, List<String> missingList){
		addTripleToMissingList(subj+"\t"+pred+"\t"+obj, missingList);
	}
	
	private void addTripleToMissingList(String triple, List<String> missingList){
		missingList.add(triple);
	}
	
	private void saveResults() throws IOException{
		StringBuilder sb = new StringBuilder();
		sb.append("RDF triples present in ontology A but not in Ontology B:\n\n");
		for(String triple : missingTriplesInB){
			sb.append(triple+"\n");
		}

		sb.append("\n*************************************************************\n\n");
		
		
		sb.append("RDF triples present in ontology B but not in Ontology A:\n\n");
		for(String triple : missingTriplesInA){
			sb.append(triple+"\n");
		}
		
		FileWriter fstream = new FileWriter(outputFile);
		BufferedWriter out = new BufferedWriter(fstream);
		out.write(sb.toString());
		// Close the output stream
		out.close();
	}
}
