package it.uniroma2.art.coda.imdb.structures;

public class ImdbDBStruct {

	private String sitoIMDBFromDB;
	private int voto;
	private String varie;
	private int numDVD;

	public ImdbDBStruct(String sitoIMDBFromDB, int voto, String varie, int numDVD) {
		super();
		this.sitoIMDBFromDB = sitoIMDBFromDB;
		this.voto = voto;
		this.varie = varie;
		this.numDVD = numDVD;
	}

	public String getSitoIMDBFromDB() {
		return sitoIMDBFromDB;
	}

	public int getVoto() {
		return voto;
	}

	public String getVarie() {
		return varie;
	}

	public int getNumDVD() {
		return numDVD;
	}

}
