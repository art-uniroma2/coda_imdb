

/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSList;


/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * XML source: D:/java_workspace/codaimdb/src/main/resources/it/uniroma2/art/coda/imdb/typeSystemDescriptor-IMBD.xml
 * @generated */
public class IMDBTVSeries extends IMDBMovie {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(IMDBTVSeries.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected IMDBTVSeries() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public IMDBTVSeries(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public IMDBTVSeries(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public IMDBTVSeries(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: creatorsList_OLD

  /** getter for creatorsList_OLD - gets 
   * @generated */
  public FSList getCreatorsList_OLD() {
    if (IMDBTVSeries_Type.featOkTst && ((IMDBTVSeries_Type)jcasType).casFeat_creatorsList_OLD == null)
      jcasType.jcas.throwFeatMissing("creatorsList_OLD", "it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
    return (FSList)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBTVSeries_Type)jcasType).casFeatCode_creatorsList_OLD)));}
    
  /** setter for creatorsList_OLD - sets  
   * @generated */
  public void setCreatorsList_OLD(FSList v) {
    if (IMDBTVSeries_Type.featOkTst && ((IMDBTVSeries_Type)jcasType).casFeat_creatorsList_OLD == null)
      jcasType.jcas.throwFeatMissing("creatorsList_OLD", "it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBTVSeries_Type)jcasType).casFeatCode_creatorsList_OLD, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: endYear

  /** getter for endYear - gets 
   * @generated */
  public int getEndYear() {
    if (IMDBTVSeries_Type.featOkTst && ((IMDBTVSeries_Type)jcasType).casFeat_endYear == null)
      jcasType.jcas.throwFeatMissing("endYear", "it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
    return jcasType.ll_cas.ll_getIntValue(addr, ((IMDBTVSeries_Type)jcasType).casFeatCode_endYear);}
    
  /** setter for endYear - sets  
   * @generated */
  public void setEndYear(int v) {
    if (IMDBTVSeries_Type.featOkTst && ((IMDBTVSeries_Type)jcasType).casFeat_endYear == null)
      jcasType.jcas.throwFeatMissing("endYear", "it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
    jcasType.ll_cas.ll_setIntValue(addr, ((IMDBTVSeries_Type)jcasType).casFeatCode_endYear, v);}    
  }

    