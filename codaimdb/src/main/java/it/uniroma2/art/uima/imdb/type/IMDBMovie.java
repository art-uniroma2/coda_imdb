

/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSList;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * XML source: D:/java_workspace/codaimdb/src/main/resources/it/uniroma2/art/coda/imdb/typeSystemDescriptor-IMBD.xml
 * @generated */
public class IMDBMovie extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(IMDBMovie.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected IMDBMovie() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public IMDBMovie(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public IMDBMovie(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public IMDBMovie(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: title

  /** getter for title - gets 
   * @generated */
  public String getTitle() {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_title == null)
      jcasType.jcas.throwFeatMissing("title", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return jcasType.ll_cas.ll_getStringValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_title);}
    
  /** setter for title - sets  
   * @generated */
  public void setTitle(String v) {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_title == null)
      jcasType.jcas.throwFeatMissing("title", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    jcasType.ll_cas.ll_setStringValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_title, v);}    
   
    
  //*--------------*
  //* Feature: year

  /** getter for year - gets 
   * @generated */
  public int getYear() {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_year == null)
      jcasType.jcas.throwFeatMissing("year", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return jcasType.ll_cas.ll_getIntValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_year);}
    
  /** setter for year - sets  
   * @generated */
  public void setYear(int v) {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_year == null)
      jcasType.jcas.throwFeatMissing("year", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    jcasType.ll_cas.ll_setIntValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_year, v);}    
   
    
  //*--------------*
  //* Feature: imdbScore

  /** getter for imdbScore - gets 
   * @generated */
  public double getImdbScore() {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_imdbScore == null)
      jcasType.jcas.throwFeatMissing("imdbScore", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_imdbScore);}
    
  /** setter for imdbScore - sets  
   * @generated */
  public void setImdbScore(double v) {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_imdbScore == null)
      jcasType.jcas.throwFeatMissing("imdbScore", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_imdbScore, v);}    
   
    
  //*--------------*
  //* Feature: description

  /** getter for description - gets 
   * @generated */
  public String getDescription() {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_description == null)
      jcasType.jcas.throwFeatMissing("description", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return jcasType.ll_cas.ll_getStringValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_description);}
    
  /** setter for description - sets  
   * @generated */
  public void setDescription(String v) {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_description == null)
      jcasType.jcas.throwFeatMissing("description", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    jcasType.ll_cas.ll_setStringValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_description, v);}    
   
    
  //*--------------*
  //* Feature: starsList_OLD

  /** getter for starsList_OLD - gets 
   * @generated */
  public FSList getStarsList_OLD() {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_starsList_OLD == null)
      jcasType.jcas.throwFeatMissing("starsList_OLD", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return (FSList)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_starsList_OLD)));}
    
  /** setter for starsList_OLD - sets  
   * @generated */
  public void setStarsList_OLD(FSList v) {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_starsList_OLD == null)
      jcasType.jcas.throwFeatMissing("starsList_OLD", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_starsList_OLD, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: movieId_OLD

  /** getter for movieId_OLD - gets 
   * @generated */
  public String getMovieId_OLD() {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_movieId_OLD == null)
      jcasType.jcas.throwFeatMissing("movieId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return jcasType.ll_cas.ll_getStringValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_movieId_OLD);}
    
  /** setter for movieId_OLD - sets  
   * @generated */
  public void setMovieId_OLD(String v) {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_movieId_OLD == null)
      jcasType.jcas.throwFeatMissing("movieId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    jcasType.ll_cas.ll_setStringValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_movieId_OLD, v);}    
   
    
  //*--------------*
  //* Feature: movieSite

  /** getter for movieSite - gets 
   * @generated */
  public IMDBSite getMovieSite() {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_movieSite == null)
      jcasType.jcas.throwFeatMissing("movieSite", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return (IMDBSite)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_movieSite)));}
    
  /** setter for movieSite - sets  
   * @generated */
  public void setMovieSite(IMDBSite v) {
    if (IMDBMovie_Type.featOkTst && ((IMDBMovie_Type)jcasType).casFeat_movieSite == null)
      jcasType.jcas.throwFeatMissing("movieSite", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBMovie_Type)jcasType).casFeatCode_movieSite, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    