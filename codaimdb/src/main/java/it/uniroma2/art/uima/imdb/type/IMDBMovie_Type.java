
/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * @generated */
public class IMDBMovie_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (IMDBMovie_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = IMDBMovie_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new IMDBMovie(addr, IMDBMovie_Type.this);
  			   IMDBMovie_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new IMDBMovie(addr, IMDBMovie_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = IMDBMovie.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("it.uniroma2.art.uima.imdb.type.IMDBMovie");
 
  /** @generated */
  final Feature casFeat_title;
  /** @generated */
  final int     casFeatCode_title;
  /** @generated */ 
  public String getTitle(int addr) {
        if (featOkTst && casFeat_title == null)
      jcas.throwFeatMissing("title", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return ll_cas.ll_getStringValue(addr, casFeatCode_title);
  }
  /** @generated */    
  public void setTitle(int addr, String v) {
        if (featOkTst && casFeat_title == null)
      jcas.throwFeatMissing("title", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    ll_cas.ll_setStringValue(addr, casFeatCode_title, v);}
    
  
 
  /** @generated */
  final Feature casFeat_year;
  /** @generated */
  final int     casFeatCode_year;
  /** @generated */ 
  public int getYear(int addr) {
        if (featOkTst && casFeat_year == null)
      jcas.throwFeatMissing("year", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return ll_cas.ll_getIntValue(addr, casFeatCode_year);
  }
  /** @generated */    
  public void setYear(int addr, int v) {
        if (featOkTst && casFeat_year == null)
      jcas.throwFeatMissing("year", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    ll_cas.ll_setIntValue(addr, casFeatCode_year, v);}
    
  
 
  /** @generated */
  final Feature casFeat_imdbScore;
  /** @generated */
  final int     casFeatCode_imdbScore;
  /** @generated */ 
  public double getImdbScore(int addr) {
        if (featOkTst && casFeat_imdbScore == null)
      jcas.throwFeatMissing("imdbScore", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return ll_cas.ll_getDoubleValue(addr, casFeatCode_imdbScore);
  }
  /** @generated */    
  public void setImdbScore(int addr, double v) {
        if (featOkTst && casFeat_imdbScore == null)
      jcas.throwFeatMissing("imdbScore", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    ll_cas.ll_setDoubleValue(addr, casFeatCode_imdbScore, v);}
    
  
 
  /** @generated */
  final Feature casFeat_description;
  /** @generated */
  final int     casFeatCode_description;
  /** @generated */ 
  public String getDescription(int addr) {
        if (featOkTst && casFeat_description == null)
      jcas.throwFeatMissing("description", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return ll_cas.ll_getStringValue(addr, casFeatCode_description);
  }
  /** @generated */    
  public void setDescription(int addr, String v) {
        if (featOkTst && casFeat_description == null)
      jcas.throwFeatMissing("description", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    ll_cas.ll_setStringValue(addr, casFeatCode_description, v);}
    
  
 
  /** @generated */
  final Feature casFeat_starsList_OLD;
  /** @generated */
  final int     casFeatCode_starsList_OLD;
  /** @generated */ 
  public int getStarsList_OLD(int addr) {
        if (featOkTst && casFeat_starsList_OLD == null)
      jcas.throwFeatMissing("starsList_OLD", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return ll_cas.ll_getRefValue(addr, casFeatCode_starsList_OLD);
  }
  /** @generated */    
  public void setStarsList_OLD(int addr, int v) {
        if (featOkTst && casFeat_starsList_OLD == null)
      jcas.throwFeatMissing("starsList_OLD", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    ll_cas.ll_setRefValue(addr, casFeatCode_starsList_OLD, v);}
    
  
 
  /** @generated */
  final Feature casFeat_movieId_OLD;
  /** @generated */
  final int     casFeatCode_movieId_OLD;
  /** @generated */ 
  public String getMovieId_OLD(int addr) {
        if (featOkTst && casFeat_movieId_OLD == null)
      jcas.throwFeatMissing("movieId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return ll_cas.ll_getStringValue(addr, casFeatCode_movieId_OLD);
  }
  /** @generated */    
  public void setMovieId_OLD(int addr, String v) {
        if (featOkTst && casFeat_movieId_OLD == null)
      jcas.throwFeatMissing("movieId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    ll_cas.ll_setStringValue(addr, casFeatCode_movieId_OLD, v);}
    
  
 
  /** @generated */
  final Feature casFeat_movieSite;
  /** @generated */
  final int     casFeatCode_movieSite;
  /** @generated */ 
  public int getMovieSite(int addr) {
        if (featOkTst && casFeat_movieSite == null)
      jcas.throwFeatMissing("movieSite", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    return ll_cas.ll_getRefValue(addr, casFeatCode_movieSite);
  }
  /** @generated */    
  public void setMovieSite(int addr, int v) {
        if (featOkTst && casFeat_movieSite == null)
      jcas.throwFeatMissing("movieSite", "it.uniroma2.art.uima.imdb.type.IMDBMovie");
    ll_cas.ll_setRefValue(addr, casFeatCode_movieSite, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public IMDBMovie_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_title = jcas.getRequiredFeatureDE(casType, "title", "uima.cas.String", featOkTst);
    casFeatCode_title  = (null == casFeat_title) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_title).getCode();

 
    casFeat_year = jcas.getRequiredFeatureDE(casType, "year", "uima.cas.Integer", featOkTst);
    casFeatCode_year  = (null == casFeat_year) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_year).getCode();

 
    casFeat_imdbScore = jcas.getRequiredFeatureDE(casType, "imdbScore", "uima.cas.Double", featOkTst);
    casFeatCode_imdbScore  = (null == casFeat_imdbScore) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_imdbScore).getCode();

 
    casFeat_description = jcas.getRequiredFeatureDE(casType, "description", "uima.cas.String", featOkTst);
    casFeatCode_description  = (null == casFeat_description) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_description).getCode();

 
    casFeat_starsList_OLD = jcas.getRequiredFeatureDE(casType, "starsList_OLD", "uima.cas.FSList", featOkTst);
    casFeatCode_starsList_OLD  = (null == casFeat_starsList_OLD) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_starsList_OLD).getCode();

 
    casFeat_movieId_OLD = jcas.getRequiredFeatureDE(casType, "movieId_OLD", "uima.cas.String", featOkTst);
    casFeatCode_movieId_OLD  = (null == casFeat_movieId_OLD) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_movieId_OLD).getCode();

 
    casFeat_movieSite = jcas.getRequiredFeatureDE(casType, "movieSite", "it.uniroma2.art.uima.imdb.type.IMDBSite", featOkTst);
    casFeatCode_movieSite  = (null == casFeat_movieSite) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_movieSite).getCode();

  }
}



    