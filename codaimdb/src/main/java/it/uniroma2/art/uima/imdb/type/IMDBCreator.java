

/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;



/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * XML source: D:/java_workspace/codaimdb/src/main/resources/it/uniroma2/art/coda/imdb/typeSystemDescriptor-IMBD.xml
 * @generated */
public class IMDBCreator extends IMDBPerson {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(IMDBCreator.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected IMDBCreator() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public IMDBCreator(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public IMDBCreator(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public IMDBCreator(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
}

    