

/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSList;
import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * XML source: D:/java_workspace/codaimdb/src/main/resources/it/uniroma2/art/coda/imdb/typeSystemDescriptor-IMBD.xml
 * @generated */
public class IMDBFilmCast extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(IMDBFilmCast.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected IMDBFilmCast() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public IMDBFilmCast(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public IMDBFilmCast(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public IMDBFilmCast(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: actorList

  /** getter for actorList - gets 
   * @generated */
  public FSList getActorList() {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_actorList == null)
      jcasType.jcas.throwFeatMissing("actorList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return (FSList)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_actorList)));}
    
  /** setter for actorList - sets  
   * @generated */
  public void setActorList(FSList v) {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_actorList == null)
      jcasType.jcas.throwFeatMissing("actorList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_actorList, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: movieId_OLD

  /** getter for movieId_OLD - gets 
   * @generated */
  public String getMovieId_OLD() {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_movieId_OLD == null)
      jcasType.jcas.throwFeatMissing("movieId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return jcasType.ll_cas.ll_getStringValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_movieId_OLD);}
    
  /** setter for movieId_OLD - sets  
   * @generated */
  public void setMovieId_OLD(String v) {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_movieId_OLD == null)
      jcasType.jcas.throwFeatMissing("movieId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    jcasType.ll_cas.ll_setStringValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_movieId_OLD, v);}    
   
    
  //*--------------*
  //* Feature: writersList

  /** getter for writersList - gets 
   * @generated */
  public FSList getWritersList() {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_writersList == null)
      jcasType.jcas.throwFeatMissing("writersList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return (FSList)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_writersList)));}
    
  /** setter for writersList - sets  
   * @generated */
  public void setWritersList(FSList v) {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_writersList == null)
      jcasType.jcas.throwFeatMissing("writersList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_writersList, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: directorsList

  /** getter for directorsList - gets 
   * @generated */
  public FSList getDirectorsList() {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_directorsList == null)
      jcasType.jcas.throwFeatMissing("directorsList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return (FSList)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_directorsList)));}
    
  /** setter for directorsList - sets  
   * @generated */
  public void setDirectorsList(FSList v) {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_directorsList == null)
      jcasType.jcas.throwFeatMissing("directorsList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_directorsList, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: movieSite

  /** getter for movieSite - gets 
   * @generated */
  public IMDBSite getMovieSite() {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_movieSite == null)
      jcasType.jcas.throwFeatMissing("movieSite", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return (IMDBSite)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_movieSite)));}
    
  /** setter for movieSite - sets  
   * @generated */
  public void setMovieSite(IMDBSite v) {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_movieSite == null)
      jcasType.jcas.throwFeatMissing("movieSite", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_movieSite, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: movie

  /** getter for movie - gets 
   * @generated */
  public IMDBMovie getMovie() {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_movie == null)
      jcasType.jcas.throwFeatMissing("movie", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return (IMDBMovie)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_movie)));}
    
  /** setter for movie - sets  
   * @generated */
  public void setMovie(IMDBMovie v) {
    if (IMDBFilmCast_Type.featOkTst && ((IMDBFilmCast_Type)jcasType).casFeat_movie == null)
      jcasType.jcas.throwFeatMissing("movie", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBFilmCast_Type)jcasType).casFeatCode_movie, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    