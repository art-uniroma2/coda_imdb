

/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * XML source: D:/java_workspace/codaimdb/src/main/resources/it/uniroma2/art/coda/imdb/typeSystemDescriptor-IMBD.xml
 * @generated */
public class IMDBPerson extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(IMDBPerson.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected IMDBPerson() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public IMDBPerson(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public IMDBPerson(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public IMDBPerson(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: imdbSite

  /** getter for imdbSite - gets 
   * @generated */
  public IMDBSite getImdbSite() {
    if (IMDBPerson_Type.featOkTst && ((IMDBPerson_Type)jcasType).casFeat_imdbSite == null)
      jcasType.jcas.throwFeatMissing("imdbSite", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    return (IMDBSite)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((IMDBPerson_Type)jcasType).casFeatCode_imdbSite)));}
    
  /** setter for imdbSite - sets  
   * @generated */
  public void setImdbSite(IMDBSite v) {
    if (IMDBPerson_Type.featOkTst && ((IMDBPerson_Type)jcasType).casFeat_imdbSite == null)
      jcasType.jcas.throwFeatMissing("imdbSite", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    jcasType.ll_cas.ll_setRefValue(addr, ((IMDBPerson_Type)jcasType).casFeatCode_imdbSite, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: name

  /** getter for name - gets 
   * @generated */
  public String getName() {
    if (IMDBPerson_Type.featOkTst && ((IMDBPerson_Type)jcasType).casFeat_name == null)
      jcasType.jcas.throwFeatMissing("name", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    return jcasType.ll_cas.ll_getStringValue(addr, ((IMDBPerson_Type)jcasType).casFeatCode_name);}
    
  /** setter for name - sets  
   * @generated */
  public void setName(String v) {
    if (IMDBPerson_Type.featOkTst && ((IMDBPerson_Type)jcasType).casFeat_name == null)
      jcasType.jcas.throwFeatMissing("name", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    jcasType.ll_cas.ll_setStringValue(addr, ((IMDBPerson_Type)jcasType).casFeatCode_name, v);}    
   
    
  //*--------------*
  //* Feature: personId_OLD

  /** getter for personId_OLD - gets 
   * @generated */
  public String getPersonId_OLD() {
    if (IMDBPerson_Type.featOkTst && ((IMDBPerson_Type)jcasType).casFeat_personId_OLD == null)
      jcasType.jcas.throwFeatMissing("personId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    return jcasType.ll_cas.ll_getStringValue(addr, ((IMDBPerson_Type)jcasType).casFeatCode_personId_OLD);}
    
  /** setter for personId_OLD - sets  
   * @generated */
  public void setPersonId_OLD(String v) {
    if (IMDBPerson_Type.featOkTst && ((IMDBPerson_Type)jcasType).casFeat_personId_OLD == null)
      jcasType.jcas.throwFeatMissing("personId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    jcasType.ll_cas.ll_setStringValue(addr, ((IMDBPerson_Type)jcasType).casFeatCode_personId_OLD, v);}    
  }

    