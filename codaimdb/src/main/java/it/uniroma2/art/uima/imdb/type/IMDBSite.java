

/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * XML source: D:/java_workspace/codaimdb/src/main/resources/it/uniroma2/art/coda/imdb/typeSystemDescriptor-IMBD.xml
 * @generated */
public class IMDBSite extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(IMDBSite.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected IMDBSite() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public IMDBSite(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public IMDBSite(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: site

  /** getter for site - gets 
   * @generated */
  public String getSite() {
    if (IMDBSite_Type.featOkTst && ((IMDBSite_Type)jcasType).casFeat_site == null)
      jcasType.jcas.throwFeatMissing("site", "it.uniroma2.art.uima.imdb.type.IMDBSite");
    return jcasType.ll_cas.ll_getStringValue(addr, ((IMDBSite_Type)jcasType).casFeatCode_site);}
    
  /** setter for site - sets  
   * @generated */
  public void setSite(String v) {
    if (IMDBSite_Type.featOkTst && ((IMDBSite_Type)jcasType).casFeat_site == null)
      jcasType.jcas.throwFeatMissing("site", "it.uniroma2.art.uima.imdb.type.IMDBSite");
    jcasType.ll_cas.ll_setStringValue(addr, ((IMDBSite_Type)jcasType).casFeatCode_site, v);}    
  }

    