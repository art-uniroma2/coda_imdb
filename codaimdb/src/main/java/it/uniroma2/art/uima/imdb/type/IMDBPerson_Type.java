
/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * @generated */
public class IMDBPerson_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (IMDBPerson_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = IMDBPerson_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new IMDBPerson(addr, IMDBPerson_Type.this);
  			   IMDBPerson_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new IMDBPerson(addr, IMDBPerson_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = IMDBPerson.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("it.uniroma2.art.uima.imdb.type.IMDBPerson");
 
  /** @generated */
  final Feature casFeat_imdbSite;
  /** @generated */
  final int     casFeatCode_imdbSite;
  /** @generated */ 
  public int getImdbSite(int addr) {
        if (featOkTst && casFeat_imdbSite == null)
      jcas.throwFeatMissing("imdbSite", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    return ll_cas.ll_getRefValue(addr, casFeatCode_imdbSite);
  }
  /** @generated */    
  public void setImdbSite(int addr, int v) {
        if (featOkTst && casFeat_imdbSite == null)
      jcas.throwFeatMissing("imdbSite", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    ll_cas.ll_setRefValue(addr, casFeatCode_imdbSite, v);}
    
  
 
  /** @generated */
  final Feature casFeat_name;
  /** @generated */
  final int     casFeatCode_name;
  /** @generated */ 
  public String getName(int addr) {
        if (featOkTst && casFeat_name == null)
      jcas.throwFeatMissing("name", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    return ll_cas.ll_getStringValue(addr, casFeatCode_name);
  }
  /** @generated */    
  public void setName(int addr, String v) {
        if (featOkTst && casFeat_name == null)
      jcas.throwFeatMissing("name", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    ll_cas.ll_setStringValue(addr, casFeatCode_name, v);}
    
  
 
  /** @generated */
  final Feature casFeat_personId_OLD;
  /** @generated */
  final int     casFeatCode_personId_OLD;
  /** @generated */ 
  public String getPersonId_OLD(int addr) {
        if (featOkTst && casFeat_personId_OLD == null)
      jcas.throwFeatMissing("personId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    return ll_cas.ll_getStringValue(addr, casFeatCode_personId_OLD);
  }
  /** @generated */    
  public void setPersonId_OLD(int addr, String v) {
        if (featOkTst && casFeat_personId_OLD == null)
      jcas.throwFeatMissing("personId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBPerson");
    ll_cas.ll_setStringValue(addr, casFeatCode_personId_OLD, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public IMDBPerson_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_imdbSite = jcas.getRequiredFeatureDE(casType, "imdbSite", "it.uniroma2.art.uima.imdb.type.IMDBSite", featOkTst);
    casFeatCode_imdbSite  = (null == casFeat_imdbSite) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_imdbSite).getCode();

 
    casFeat_name = jcas.getRequiredFeatureDE(casType, "name", "uima.cas.String", featOkTst);
    casFeatCode_name  = (null == casFeat_name) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_name).getCode();

 
    casFeat_personId_OLD = jcas.getRequiredFeatureDE(casType, "personId_OLD", "uima.cas.String", featOkTst);
    casFeatCode_personId_OLD  = (null == casFeat_personId_OLD) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_personId_OLD).getCode();

  }
}



    