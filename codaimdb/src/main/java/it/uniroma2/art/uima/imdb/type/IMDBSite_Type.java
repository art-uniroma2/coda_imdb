
/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * @generated */
public class IMDBSite_Type extends TOP_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (IMDBSite_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = IMDBSite_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new IMDBSite(addr, IMDBSite_Type.this);
  			   IMDBSite_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new IMDBSite(addr, IMDBSite_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = IMDBSite.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("it.uniroma2.art.uima.imdb.type.IMDBSite");
 
  /** @generated */
  final Feature casFeat_site;
  /** @generated */
  final int     casFeatCode_site;
  /** @generated */ 
  public String getSite(int addr) {
        if (featOkTst && casFeat_site == null)
      jcas.throwFeatMissing("site", "it.uniroma2.art.uima.imdb.type.IMDBSite");
    return ll_cas.ll_getStringValue(addr, casFeatCode_site);
  }
  /** @generated */    
  public void setSite(int addr, String v) {
        if (featOkTst && casFeat_site == null)
      jcas.throwFeatMissing("site", "it.uniroma2.art.uima.imdb.type.IMDBSite");
    ll_cas.ll_setStringValue(addr, casFeatCode_site, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public IMDBSite_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_site = jcas.getRequiredFeatureDE(casType, "site", "uima.cas.String", featOkTst);
    casFeatCode_site  = (null == casFeat_site) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_site).getCode();

  }
}



    