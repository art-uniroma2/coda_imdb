
/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * @generated */
public class IMDBTVSeries_Type extends IMDBMovie_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (IMDBTVSeries_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = IMDBTVSeries_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new IMDBTVSeries(addr, IMDBTVSeries_Type.this);
  			   IMDBTVSeries_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new IMDBTVSeries(addr, IMDBTVSeries_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = IMDBTVSeries.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
 
  /** @generated */
  final Feature casFeat_creatorsList_OLD;
  /** @generated */
  final int     casFeatCode_creatorsList_OLD;
  /** @generated */ 
  public int getCreatorsList_OLD(int addr) {
        if (featOkTst && casFeat_creatorsList_OLD == null)
      jcas.throwFeatMissing("creatorsList_OLD", "it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
    return ll_cas.ll_getRefValue(addr, casFeatCode_creatorsList_OLD);
  }
  /** @generated */    
  public void setCreatorsList_OLD(int addr, int v) {
        if (featOkTst && casFeat_creatorsList_OLD == null)
      jcas.throwFeatMissing("creatorsList_OLD", "it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
    ll_cas.ll_setRefValue(addr, casFeatCode_creatorsList_OLD, v);}
    
  
 
  /** @generated */
  final Feature casFeat_endYear;
  /** @generated */
  final int     casFeatCode_endYear;
  /** @generated */ 
  public int getEndYear(int addr) {
        if (featOkTst && casFeat_endYear == null)
      jcas.throwFeatMissing("endYear", "it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
    return ll_cas.ll_getIntValue(addr, casFeatCode_endYear);
  }
  /** @generated */    
  public void setEndYear(int addr, int v) {
        if (featOkTst && casFeat_endYear == null)
      jcas.throwFeatMissing("endYear", "it.uniroma2.art.uima.imdb.type.IMDBTVSeries");
    ll_cas.ll_setIntValue(addr, casFeatCode_endYear, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public IMDBTVSeries_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_creatorsList_OLD = jcas.getRequiredFeatureDE(casType, "creatorsList_OLD", "uima.cas.FSList", featOkTst);
    casFeatCode_creatorsList_OLD  = (null == casFeat_creatorsList_OLD) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_creatorsList_OLD).getCode();

 
    casFeat_endYear = jcas.getRequiredFeatureDE(casType, "endYear", "uima.cas.Integer", featOkTst);
    casFeatCode_endYear  = (null == casFeat_endYear) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_endYear).getCode();

  }
}



    