
/* First created by JCasGen Wed Jul 30 15:10:35 CEST 2014 */
package it.uniroma2.art.uima.imdb.type;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Mon Aug 11 22:03:36 CEST 2014
 * @generated */
public class IMDBFilmCast_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (IMDBFilmCast_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = IMDBFilmCast_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new IMDBFilmCast(addr, IMDBFilmCast_Type.this);
  			   IMDBFilmCast_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new IMDBFilmCast(addr, IMDBFilmCast_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = IMDBFilmCast.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
 
  /** @generated */
  final Feature casFeat_actorList;
  /** @generated */
  final int     casFeatCode_actorList;
  /** @generated */ 
  public int getActorList(int addr) {
        if (featOkTst && casFeat_actorList == null)
      jcas.throwFeatMissing("actorList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return ll_cas.ll_getRefValue(addr, casFeatCode_actorList);
  }
  /** @generated */    
  public void setActorList(int addr, int v) {
        if (featOkTst && casFeat_actorList == null)
      jcas.throwFeatMissing("actorList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    ll_cas.ll_setRefValue(addr, casFeatCode_actorList, v);}
    
  
 
  /** @generated */
  final Feature casFeat_movieId_OLD;
  /** @generated */
  final int     casFeatCode_movieId_OLD;
  /** @generated */ 
  public String getMovieId_OLD(int addr) {
        if (featOkTst && casFeat_movieId_OLD == null)
      jcas.throwFeatMissing("movieId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return ll_cas.ll_getStringValue(addr, casFeatCode_movieId_OLD);
  }
  /** @generated */    
  public void setMovieId_OLD(int addr, String v) {
        if (featOkTst && casFeat_movieId_OLD == null)
      jcas.throwFeatMissing("movieId_OLD", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    ll_cas.ll_setStringValue(addr, casFeatCode_movieId_OLD, v);}
    
  
 
  /** @generated */
  final Feature casFeat_writersList;
  /** @generated */
  final int     casFeatCode_writersList;
  /** @generated */ 
  public int getWritersList(int addr) {
        if (featOkTst && casFeat_writersList == null)
      jcas.throwFeatMissing("writersList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return ll_cas.ll_getRefValue(addr, casFeatCode_writersList);
  }
  /** @generated */    
  public void setWritersList(int addr, int v) {
        if (featOkTst && casFeat_writersList == null)
      jcas.throwFeatMissing("writersList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    ll_cas.ll_setRefValue(addr, casFeatCode_writersList, v);}
    
  
 
  /** @generated */
  final Feature casFeat_directorsList;
  /** @generated */
  final int     casFeatCode_directorsList;
  /** @generated */ 
  public int getDirectorsList(int addr) {
        if (featOkTst && casFeat_directorsList == null)
      jcas.throwFeatMissing("directorsList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return ll_cas.ll_getRefValue(addr, casFeatCode_directorsList);
  }
  /** @generated */    
  public void setDirectorsList(int addr, int v) {
        if (featOkTst && casFeat_directorsList == null)
      jcas.throwFeatMissing("directorsList", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    ll_cas.ll_setRefValue(addr, casFeatCode_directorsList, v);}
    
  
 
  /** @generated */
  final Feature casFeat_movieSite;
  /** @generated */
  final int     casFeatCode_movieSite;
  /** @generated */ 
  public int getMovieSite(int addr) {
        if (featOkTst && casFeat_movieSite == null)
      jcas.throwFeatMissing("movieSite", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return ll_cas.ll_getRefValue(addr, casFeatCode_movieSite);
  }
  /** @generated */    
  public void setMovieSite(int addr, int v) {
        if (featOkTst && casFeat_movieSite == null)
      jcas.throwFeatMissing("movieSite", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    ll_cas.ll_setRefValue(addr, casFeatCode_movieSite, v);}
    
  
 
  /** @generated */
  final Feature casFeat_movie;
  /** @generated */
  final int     casFeatCode_movie;
  /** @generated */ 
  public int getMovie(int addr) {
        if (featOkTst && casFeat_movie == null)
      jcas.throwFeatMissing("movie", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    return ll_cas.ll_getRefValue(addr, casFeatCode_movie);
  }
  /** @generated */    
  public void setMovie(int addr, int v) {
        if (featOkTst && casFeat_movie == null)
      jcas.throwFeatMissing("movie", "it.uniroma2.art.uima.imdb.type.IMDBFilmCast");
    ll_cas.ll_setRefValue(addr, casFeatCode_movie, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public IMDBFilmCast_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_actorList = jcas.getRequiredFeatureDE(casType, "actorList", "uima.cas.FSList", featOkTst);
    casFeatCode_actorList  = (null == casFeat_actorList) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_actorList).getCode();

 
    casFeat_movieId_OLD = jcas.getRequiredFeatureDE(casType, "movieId_OLD", "uima.cas.String", featOkTst);
    casFeatCode_movieId_OLD  = (null == casFeat_movieId_OLD) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_movieId_OLD).getCode();

 
    casFeat_writersList = jcas.getRequiredFeatureDE(casType, "writersList", "uima.cas.FSList", featOkTst);
    casFeatCode_writersList  = (null == casFeat_writersList) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_writersList).getCode();

 
    casFeat_directorsList = jcas.getRequiredFeatureDE(casType, "directorsList", "uima.cas.FSList", featOkTst);
    casFeatCode_directorsList  = (null == casFeat_directorsList) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_directorsList).getCode();

 
    casFeat_movieSite = jcas.getRequiredFeatureDE(casType, "movieSite", "it.uniroma2.art.uima.imdb.type.IMDBSite", featOkTst);
    casFeatCode_movieSite  = (null == casFeat_movieSite) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_movieSite).getCode();

 
    casFeat_movie = jcas.getRequiredFeatureDE(casType, "movie", "it.uniroma2.art.uima.imdb.type.IMDBMovie", featOkTst);
    casFeatCode_movie  = (null == casFeat_movie) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_movie).getCode();

  }
}



    